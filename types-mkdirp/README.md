This is a stub types definition for @types/mkdirp (https://github.com/isaacs/node-mkdirp#readme).

mkdirp provides its own type definitions, so you don't need @types/mkdirp installed!